
// User
{ 	
	"userId" = "user_001"
	"firstName": "Vanessa",
	"lastName": "Hidalgo",
	"email": "vhidalgo@mail.com",
	"password": "password123",
	"isAdmin": false,
	"mobileNumber": "09175600876",
 }

 // Products

 {
 	"productId" = "boots_001"
 	"name": "boots",
 	"description": "white leather boots",
 	"price": 1200,
 	"stocks": 40,
 	"isActive": true,
 	"SKU": "431WKKYZ"
 }

 // OrderProducts

 {
 	"orderProductsId" = "orderproduct_001"
 	"orderId": "order_001",
 	"productId": "boots_001",
 	"quantity": 2,
 	"price": 1200
 	"subtotal": /quantity * /price
 }

  // Orders

 {
 	"orderId": "order_001",
 	"userId": "user_001"
 	"status": "Successful"
 	"total": 1,
 }
